<img src="./assets/wallpaperbot.svg" alt="logo" width="200"/>

# Wallpaper bot for Mastodon

Posts random wallpaper fetched from [Wallhaven](https://wallhaven.cc) to Mastodon

<a rel="me" href="https://mastodon.social/@wallpapers">Official bot</a>

# Configuration

To run this bot, you first need to create an account on [Mastodon](https://mastodon.social) and get an access token for your account.

Next you need to make the .env file with the following content:

```env
INSTANCE_URL=https://mastodon.social
MASTODON_TOKEN=yourtokenhere
VISIBILITY=public
```

Fill the INSTANCE_URL and MASTODON_TOKEN with the correct values.

## Scheduling

This bot no longer does need to run 24/7, the posts can be made at a single time and posted in the future.

Add this into your .env file if you want to enable scheduling:

```env
SCHEDULE_ENABLE=true
SCHEDULE_INTERVAL=4h
SCHEDULE_DURATION=1d
```

This will post every 4 hours for one day.

## Optional configuration

You can also add the following to your .env file:

```env
# Optional
NTFY_HOST=https://ntfy.sh
NTFY_ROOM=your-ntfy-room
```

This will send all errors to the specified ntfy room.

## Testing

If you need to test something without having to post anything to Mastodon, add the following to your .env file:

```env
TESTING=true
```

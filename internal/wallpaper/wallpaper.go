package wallpaper

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"codeberg.org/tomkoid/wallpaper-bot-mastodon/internal/mastodon"
	"codeberg.org/tomkoid/wallpaper-bot-mastodon/internal/utils"
)

var attempts int = 0

// returns if the error is nil
func check(err error) bool {
	if err != nil {
		utils.LogError(err, false)

		attempts++
		if attempts > 3 {
			utils.LogError(errors.New("Too many attempts"), true)
		}
	}

	return (err == nil)
}

type scheduleResponse struct {
	loopCount     int
	scheduleTimes []string
}

func getScheduleEnvConfig() (time.Duration, time.Duration) {
	tmpScheduleInterval, err := utils.ParseEnvDuration(os.Getenv("SCHEDULE_INTERVAL"))

	if err != nil {
		utils.LogError(errors.New("could not parse SCHEDULE_INTERVAL"), true)
	}

	tmpScheduleDuration, err := utils.ParseEnvDuration(os.Getenv("SCHEDULE_DURATION"))

	if err != nil {
		utils.LogError(errors.New("could not parse SCHEDULE_DURATION"), true)
	}

	return *tmpScheduleInterval, *tmpScheduleDuration
}

func scheduleWallpapers(schedule bool) scheduleResponse {
	var scheduleInterval time.Duration = time.Duration(0)
	var scheduleDuration time.Duration = time.Duration(0)

	if os.Getenv("SCHEDULE_INTERVAL") == "" || os.Getenv("SCHEDULE_DURATION") == "" {
		utils.LogError(
			errors.New(
				"the SCHEDULE_INTERVAL or SCHEDULE_DURATION option is not set in .env. please check the readme for usage.",
			),
			true,
		)
	}

	if schedule {
		scheduleInterval, scheduleDuration = getScheduleEnvConfig()

		wallpaperLoopCount := int(
			scheduleDuration / scheduleInterval,
		)

		t := time.Now()
		now := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
		trueNow := time.Now()
		roundSpec := time.Hour

		times := make([]string, 0)

		i := 0
		trueI := 0
		maxI := wallpaperLoopCount
		for {
			correctTime := now.Add(scheduleInterval * time.Duration(trueI)).Round(roundSpec)

			// check if the time of posting hasn't already passed
			// ex.: now returns 19:00 and trueNow returns 19:05, you can't post in the past
			if !correctTime.Before(trueNow) {
				times = append(times, correctTime.Format(time.RFC3339))
				i++
			}

			trueI++

			if i == maxI {
				break
			}
		}

		if len(times) != wallpaperLoopCount {
			utils.LogError(
				errors.New("number of timestamps doesnt correspond with wallpaperLoopCount"),
				true,
			)
		}

		// fmt.Printf("now: %v", trueNow.String())
		// fmt.Printf("times: %v", times)

		return scheduleResponse{
			loopCount:     wallpaperLoopCount,
			scheduleTimes: times,
		}
	} else {
		return scheduleResponse{
			loopCount:     0,
			scheduleTimes: make([]string, 0),
		}
	}
}

// Posts a random wallpaper to Mastodon
func PostWallpaper(schedule bool) {
	utils.SendNtfyNotification(utils.NtfyRequest{
		Title:    "Wallpaper Bot",
		Body:     "Starting..",
		Priority: 1,
	}, nil)

	scheduleResp := scheduleWallpapers(schedule)

	wallpaperLoopCount := scheduleResp.loopCount
	wallpaperPostTimes := scheduleResp.scheduleTimes
	wallpaperLoopIndex := 0

	for {
		if schedule {
			if wallpaperLoopIndex > 0 {
				fmt.Printf("%s\n", strings.Repeat("=", 40))
			}
			fmt.Printf("Pending wallpapers: %d\n", wallpaperLoopCount-wallpaperLoopIndex)
		}

		for {
			var schedulePostTime *string = nil

			if schedule {
				schedulePostTime = &wallpaperPostTimes[wallpaperLoopIndex]

				inCache, err := utils.IsInCache(*schedulePostTime)

				if ok := check(err); !ok {
					utils.LogError(errors.New("could not check if date is in cache"), true)
				}

				if inCache {
					utils.Log(
						"Already in cache. Please remove the cache if you want to repost the statuses.",
					)
					break
				}
			}

			// get a random wallpaper from Wallhaven API
			utils.Log("Getting wallpaper..")
			wallpaper, err := GetWallpaper()
			if ok := check(err); !ok {
				continue
			}

			utils.Log("Downloading wallpaper..")
			wallpaperImage, err := GetWallpaperImage(wallpaper)
			if ok := check(err); !ok {
				continue
			}

			fmt.Print("Getting wallpaper tags..")
			wallpaperTags, err := GetWallpaperTags(wallpaper)
			if ok := check(err); !ok {
				continue
			}

			// post the wallpaper to Mastodon using the Mastodon API
			if os.Getenv("TESTING") != "true" {
				utils.Log("Posting wallpaper..")
				mediaID, err := mastodon.PostMedia(wallpaperImage, wallpaperTags)
				if ok := check(err); !ok {
					continue
				}

				// post the status to Mastodon using the Mastodon API
				utils.Log("Posting status..")
				err = mastodon.PostStatus(wallpaper, mediaID, schedulePostTime)
				if ok := check(err); !ok {
					continue
				}

			}

			if schedule {
				utils.Log(fmt.Sprintf("Successfully scheduled status to %s", *schedulePostTime))

				err = utils.AddDateToCache(*schedulePostTime)

				if ok := check(err); !ok {
					utils.LogError(errors.New("could not add date to cache"), true)
				}
			}

			break
		}

		utils.SendNtfyNotification(utils.NtfyRequest{
			Title:    "Wallpaper Bot",
			Body:     "Posted wallpaper!",
			Priority: 2,
		}, nil)

		if !schedule {
			break
		} else {
			wallpaperLoopIndex = wallpaperLoopIndex + 1
			if wallpaperLoopIndex == wallpaperLoopCount {
				break
			}
		}
	}
}

package wallpaper

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/http"

	"codeberg.org/tomkoid/wallpaper-bot-mastodon/internal/models"
)

/*
Gets a random list of wallpapers from Wallhaven API
https://wallhaven.cc/api/v1/search?sorting=random
*/
func GetWallhavenWallpapers() ([]byte, error) {
	wallpapers, err := http.Get("https://wallhaven.cc/api/v1/search?sorting=random")
	if err != nil {
		return nil, err
	}

	defer wallpapers.Body.Close()

	body, err := io.ReadAll(wallpapers.Body)
	if err != nil {
		return nil, err
	}

	// fmt.Println(string(body))
	return body, nil
}

func GetWallpaperDetails(wallpaper models.Wallpaper) ([]byte, error) {
	wallpaperMore, err := http.Get(fmt.Sprintf("https://wallhaven.cc/api/v1/w/%s", wallpaper.Id))
	if err != nil {
		return nil, err
	}

	defer wallpaperMore.Body.Close()

	body, err := io.ReadAll(wallpaperMore.Body)
	if err != nil {
		return nil, err
	}

	// fmt.Println(string(body))
	return body, nil
}

// Parses the GetWallhavenWallpapers response and returns one random wallpaper
func GetWallpaper() (models.Wallpaper, error) {
	// get a random wallpaper from Wallhaven API
	data, err := GetWallhavenWallpapers()
	if err != nil {
		return models.Wallpaper{}, err
	}

	var wallpapers models.Wallpapers
	err = json.Unmarshal([]byte(data), &wallpapers)
	if err != nil {
		return models.Wallpaper{}, err
	}

	if len(wallpapers.Data) == 0 {
		return models.Wallpaper{}, errors.New("no wallpapers found")
	}

	randomIndex := rand.Intn(len(wallpapers.Data) - 1)

	return wallpapers.Data[randomIndex], nil
}

// Gets wallpaper's tags
func GetWallpaperTags(wallpaper models.Wallpaper) ([]string, error) {
	data, err := GetWallpaperDetails(wallpaper)
	if err != nil {
		return nil, err
	}

	var details models.WallpaperDetails
	err = json.Unmarshal([]byte(data), &details)
	if err != nil {
		return nil, err
	}

	fmt.Println(" Tags found:", len(details.Data.Tags))

	tags := make([]string, 0)
	for _, val := range details.Data.Tags {
		tags = append(tags, val.Name)
	}

	return tags, nil
}

// Downloads the wallpaper image and returns bytes of the wallpaper
func GetWallpaperImage(wallpaper models.Wallpaper) ([]byte, error) {
	wallpaperImage, err := http.Get(wallpaper.Path)
	if err != nil {
		return nil, err
	}

	defer wallpaperImage.Body.Close()

	body, err := io.ReadAll(wallpaperImage.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

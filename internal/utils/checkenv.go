package utils

import (
	"log"
	"os"
)

func CheckEnv() {
	if os.Getenv("INSTANCE_URL") == "" {
		log.Fatal("No INSTANCE_URL defined in .env file")
	}

	if os.Getenv("MASTODON_TOKEN") == "" {
		log.Fatal("No MASTODON_TOKEN defined in .env file")
	}

	if os.Getenv("VISIBILITY") == "" {
		os.Setenv("VISIBILITY", "public")
	}
}

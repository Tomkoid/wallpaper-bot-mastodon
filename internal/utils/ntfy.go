package utils

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"os"
)

type NtfyRequest struct {
	Title    string `json:"title"`
	Body     string `json:"body"`
	Priority int    `json:"priority"`
}

func ntfy(request NtfyRequest) error {
	if os.Getenv("NTFY_ROOM") == "" {
		return errors.New("NTFY_ROOM is not set")
	}
	if os.Getenv("NTFY_HOST") == "" {
		os.Setenv("NTFY_HOST", "https://ntfy.sh")
	}

	url := fmt.Sprintf("%s/%s", os.Getenv("NTFY_HOST"), os.Getenv("NTFY_ROOM"))

	// body is the string
	// reqBody, err := io.ReadAll(request)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(request.Body)))
	if err != nil {
		return err
	}

	req.Header.Set("Title", request.Title)
	req.Header.Set("Priority", fmt.Sprintf("%d", request.Priority))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	return nil
}

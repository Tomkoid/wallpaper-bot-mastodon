package models

type Wallpapers struct {
	Data []Wallpaper `json:"data"`
}

type Thumbs struct {
	Large    string `json:"large"`
	Original string `json:"original"`
	Small    string `json:"small"`
}

type Tag struct {
	Name string `json:"name"`
}

type Wallpaper struct {
	Id         string   `json:"id"`
	Url        string   `json:"url"`
	ShortUrl   string   `json:"short_url"`
	Views      int      `json:"views"`
	Favorites  int      `json:"favorites"`
	Source     string   `json:"source"`
	Purity     string   `json:"purity"`
	Category   string   `json:"category"`
	DimensionX int      `json:"dimension_x"`
	DimensionY int      `json:"dimension_y"`
	Resolution string   `json:"resolution"`
	Ratio      string   `json:"ratio"`
	FileSize   int      `json:"file_size"`
	FileType   string   `json:"file_type"`
	CreatedAt  string   `json:"created_at"`
	Colors     []string `json:"colors"`
	Path       string   `json:"path"`
	Thumbs     Thumbs   `json:"thumbs"`
}

type WallpaperDetails struct {
	Data WallpaperDetailsData `json:"data"`
}

type WallpaperDetailsData struct {
	Wallpaper
	Tags []Tag `json:"tags"`
}

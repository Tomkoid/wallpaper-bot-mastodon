package models

type Media struct {
	Id         string `json:"id"`
	Type       string `json:"type"`
	Url        string `json:"url"`
	PreviewUrl string `json:"preview_url"`
}

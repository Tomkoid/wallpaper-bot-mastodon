package mastodon

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
	"time"

	"codeberg.org/tomkoid/wallpaper-bot-mastodon/internal/models"
)

func getMediaMultipartBody(image []byte) (*bytes.Buffer, string) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)

	part, _ := writer.CreateFormFile("file", "wallpaper.jpg")

	io.Copy(part, bytes.NewReader(image))

	writer.Close()

	return body, writer.FormDataContentType()
}

func publishMedia(body *bytes.Buffer, contentType string, tags []string) (string, error) {
	var url string = fmt.Sprintf("%s/api/v2/media", os.Getenv("INSTANCE_URL"))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(body.Bytes()))
	if err != nil {
		return "", err
	}

	var auth string = "Bearer " + os.Getenv("MASTODON_TOKEN")

	req.Header.Set("Content-Type", contentType)
	req.Header.Add("Authorization", auth)

	q := req.URL.Query()

	if len(tags) != 0 {
		q.Add("description", strings.Join(tags, ", "))
	}

	req.URL.RawQuery = q.Encode()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	reqBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	remainingRateLimit := resp.Header.Get("X-Ratelimit-Remaining")
	resetRateLimit := resp.Header.Get("X-Ratelimit-Reset")
	fmt.Printf("Remaining requests for rate limit: %s, reset after %s\n", remainingRateLimit, resetRateLimit)

	if remainingRateLimit == "0" {
		fmt.Println("Rate limit reached, exiting..")
		os.Exit(1)
	}

	if resp.StatusCode != 200 {
		time.Sleep(time.Second * 3)
		fmt.Printf("body: %s\n", reqBody)
		return "", errors.New("failed to publish media, probably rate limiting?")
	}

	var media models.Media
	json.Unmarshal([]byte(reqBody), &media)

	return media.Id, nil
}

func PostMedia(image []byte, tags []string) (string, error) {
	body, contentType := getMediaMultipartBody(image)
	mediaID, err := publishMedia(body, contentType, tags)
	if err != nil {
		return "", err
	}

	return mediaID, nil
}
